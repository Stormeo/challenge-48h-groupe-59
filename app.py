#import des ressources décessaire (utiliser la commande "pip install", exemple: "pip install Flask" pour les télécharger sur votre machine)
from flask import Flask, request, url_for, render_template
from flask_pymongo import PyMongo

#initialisation des configuration de l'appli et du lien au serveur
app = Flask(__name__)
app.config['MONGO_URI'] = "mongodb+srv://Djeverson:Rocketman1@cluster0.gv12x.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
mongo = PyMongo(app)

#home page
@app.route('/')
def index():
    return render_template('form.html')

#Fonctien d'envois d'image vers le serveur
@app.route('/uploadPhoto', methods=['POST'])
def createProduct():
    titre = request.form.get("title")
    type = request.form.get("type")
    humain = request.form.get("humain")
    institutionelle = request.form.get("institutionelle")
    format = request.form.get("format")
    credits = request.form.get("credits")
    use_limit = request.form.get("use_limit")
    copyright = request.form.get("copyright")
    copyright_limit = request.form.get("copyright_limit")
    if 'productImage[]' in request.files:
        productImage = request.files.getlist('productImage[]')
        for productImage in productImage:
            mongo.save_file(productImage.filename, productImage)
            mongo.db.passionFroid.insert({'productImage_name': productImage.filename,
                                            'titre': titre,
                                            'type': type,
                                            'humain': humain,
                                            'institutionelle': institutionelle,
                                            'format': format,
                                            'credits': credits,
                                            'use_limit': use_limit,
                                            'copyright': copyright,
                                            'copyright_limit': copyright_limit
                                        })

    return render_template('form.html')

#affichage des images séparément
@app.route('/file/<filename>')
def printfile(filename):
    return mongo.db.passionFroid.find_one({'productImage_name': filename})

#lancement de l'appli
if __name__ == '__main__':
    app.run(debug=True)